# Bash-Timsort

Implementation of the Tim sort algorithm in Bash, for sorting numeric bash arrays in-place.
This is only ever faster than sorting with the `sort` command for small arrays (approx. less than 20 elements with GNU `sort`).

## Usage

```bash
source "timsort.bash"
A=(35 15 25 50 30 10 40)
timsort A
# A=(10 15 25 30 35 40 50)
```
