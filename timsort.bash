#!/usr/bin/bash

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 

function timsort/find_minrun {
	local n="$1" minr="$2" r=0
	while ((n>minr)); do
		((r|=n&1, n>>=1))
	done
	minrun=$((n+r))
}

function timsort/insertion_sort {
	# insertion_sort A #start #finish
	local -n array2="$1"
	local i j element left="$2" right="$3"
	for ((i=$left;i<$((right+1));i++)); do
		element="${array2[$i]}"
		((j=i-1))
		while ((element<${array2[$j]})) && ((j>=left));do
			array2[((j+1))]="${array2[$j]}"
			((j-=1))
		done
		array2[((j+1))]="$element"
	done
}

function timsort/merge {
	local -n array="$1"
	local l="$2" m="$3" r="$4" \
		array_length1=$((m-l+1)) array_length2=$((r-m)) \
		i j k
	local -a left=() right=()

	for ((i=0;i<=array_length1;i++));do
		left+=( "${array[((l+i))]}" )
	done
	for ((i=0;i<=array_length2;i++));do
		right+=("${array[((m+1+i))]}")
	done

	i=0 j=0 k="$l"

	while ((j<array_length2)) && ((i<array_length1)); do
		if ((${left[$i]} <= ${right[$j]})); then
			array[k]="${left[$i]}"
			((i++))
		else
			array[k]="${right[$j]}"
			((j++))
		fi
		((k++))
	done

	while ((i<array_length1)); do
		array[k]="${left[$i]}"
		((k++,i++))
	done

	while ((j<array_length2)); do
		array[k]="${right[$i]}"
		((k++,j++))
	done
}

function timsort {
	# Sort a numeric array
	# timsort A     # Sort array 'A' in-place
	# timsort A 60  # with at least the length of the array to not produce innacurate results
	local -n array3="$1"
	local minrun n="${#array3[@]}" end mid right
	timsort/find_minrun "$n" "${2:-"${#array3[@]}"}"

	for ((start=0;start<n;start+=minrun));do
		end=$((start+minrun<n?start+minrun-1:n-1))
		timsort/insertion_sort array3 start end
	done

	local size="$minrun"
	while ((size<n)); do
		for ((left=0;left<n;left+=(2*size)));do
			mid=$((n < left+size ? n-1 : left+size-1)) \
			right=$((left+2*size < n ? (left+2*size) : n-1))
			timsort/merge array3 left mid right
		done
		((size*=2))
	done
}
