#!/usr/bin/bash
source "timsort.bash"

for n in {10..25} 30 40 50 100; do
	printf -- "\033[1;34m====>\033[m n=\033[1;32m%s\033[m \033[1;34m<====\033[m" "$n"
	A=($(shuf -i 1-1000 -n "$n" | tr '\n' ' '))
	echo
	# Regular sort
	printf -- "\033[1;31mRegular sort\033[m"
	time B=($(printf -- "%s\n" "${A[@]}"|sort -n))
	printf -- "%s " "${B[@]}"
	echo
	echo
	printf -- "\033[1;31mTim sort\033[m"
	# Tim sort with bash
	time timsort A
	printf -- "%s " "${A[@]}"
	echo
	echo
done
